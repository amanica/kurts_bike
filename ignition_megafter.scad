render = true;
//render = false;
$fn=render?90:15;
minkowFn=render?31:7;

epsilon = 0.01;
roundnessPerSide = 2;
roundnessPer2Sides = 2 * roundnessPerSide;

buttonHoleD = 20; //??????

topTopD = 32;
topBottomD = 32.5;
topH = 30;

bottomTopD = 41.85 ;
bottomBottomD = 42.44 ;
bottomH = 35;

wallThickNess=2.1;
veryBottomWallThickNess=1.32;
veryBottomWallH=7.45;

screwHouseTopD=7.3;
screwHouseBottomD=9;
screwHouseH=14.17;
totalWidthWithScrewHouseBottomD=52.66;
screwDepth=screwHouseH-wallThickNess;//9.8;
screwD=4;

innerRoofArchVR=wallThickNess*2; // avoid supports by building an dome inside

union(){
    difference() {        
        minkowski() {
            union() {
                //top
                cylinder(d2=topTopD-roundnessPer2Sides, d1=topBottomD-roundnessPer2Sides, h=topH-roundnessPerSide);
               
                //bottom
                translate([0,0,-bottomH]) {
                    cylinder(d2=bottomTopD-roundnessPer2Sides, d1=bottomBottomD-roundnessPer2Sides, h=bottomH-roundnessPerSide); 
                    screwHouse(1);
                    screwHouse(-1);                        
                }
                
            }
            sphere(r=roundnessPerSide,center = true, $fn=minkowFn);     
        }
        
        // cut button hole
        #translate([0,0,topH-wallThickNess-innerRoofArchVR-epsilon]) {         
                   cylinder(d=buttonHoleD, 
                        h=wallThickNess+innerRoofArchVR+2*epsilon); 
        }
        
        #translate([0,0,-innerRoofArchVR-epsilon]) {                
                // cut out basic top
                cylinder(d2=topTopD-2*wallThickNess, d1=topBottomD-2*wallThickNess, 
                        h=topH-wallThickNess-innerRoofArchVR+innerRoofArchVR+epsilon); 
        }
        // cut out top dome
        #translate([0,0,topH-wallThickNess-innerRoofArchVR-epsilon]) {         
            resize([topTopD-2*wallThickNess,topTopD-2*wallThickNess,2*innerRoofArchVR])sphere(d=1);   
        }
                  
        translate([0,0,-bottomH]) {       
            screwThread(1);
            screwThread(-1);
             
            // cut off a flat bottom
            #translate([-totalWidthWithScrewHouseBottomD/2,-totalWidthWithScrewHouseBottomD/2,-roundnessPerSide]) 
                cube([totalWidthWithScrewHouseBottomD+epsilon,totalWidthWithScrewHouseBottomD+epsilon,roundnessPerSide+epsilon]);
            
            translate([0,0,-epsilon]) {                
             
                // cut out basic bottom
                difference() {
                    union() {
                        cylinder(d2=bottomTopD-2*wallThickNess, d1=bottomBottomD-2*wallThickNess, 
                            h=bottomH-wallThickNess-innerRoofArchVR+epsilon); 
                        cylinder(d2=bottomBottomD-2*veryBottomWallThickNess, d1=bottomBottomD-2*veryBottomWallThickNess, 
                            h=veryBottomWallH+epsilon); 

                    }
                    
                    // but don't cut out the screw houses
                    screwHouse(1,0);
                    screwHouse(-1,0); 
                }
            }
        }
        // cut out bottom dome
        translate([0,0,-wallThickNess-innerRoofArchVR-epsilon]) {         
            resize([bottomTopD-2*wallThickNess,bottomTopD-2*wallThickNess,2*innerRoofArchVR])sphere(d=1);   
        }
    }    
}

// check roundness
//translate([0,0,2]) #cylinder(d2=bottomTopD, d1=bottomTopD, h=bottomH);

module screwHouse(side=1,roundnessPer2Sides=roundnessPer2Sides) {
    translate([side * (totalWidthWithScrewHouseBottomD/2 - screwHouseBottomD/2),0,0]) 
        cylinder(d2=screwHouseTopD-roundnessPer2Sides,d1=screwHouseBottomD-roundnessPer2Sides, h=screwHouseH-roundnessPer2Sides/2);
}

module screwThread(side=1) {
    include <threads.scad>;
    translate([side * (totalWidthWithScrewHouseBottomD/2 - screwHouseBottomD/2),0,-epsilon]) 
        metric_thread(diameter=screwD, pitch=0.77, length=screwDepth, internal=true);
}        